///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// Usage:  helloworld
//
// Result:
//   Print Hello World! in g++
//
// Example:
//   Hello World!
//
// @author Nicholas Tom <tom7@hawaii.edu>
// @date   02_15_2021
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>     
#include <iostream>    

using namespace std;   

int main() {
        
   cout << "Hello" << " " << "World!" << endl; 

   return 0;
}
