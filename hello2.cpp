///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// Usage:  helloworld
//
// Result:
//   Print Hello World! in g++ without using namespace
//
// Example:
//   Hello World!
//
// @author Nicholas Tom <tom7@hawaii.edu>
// @date   02_15_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>     
#include <iostream>   


int main() {
        
   std::cout << "Hello World!" << std::endl;

   return 0;
}

